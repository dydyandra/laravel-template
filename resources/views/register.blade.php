<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Pendaftaran</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="{{ route('welcome') }}" method="POST">
        @csrf
        <label for="first-name">First name:</label><br><br>
        <input type="text" id="first-name" name="first-name"><br><br>
        <label for="last-name">Last name:</label><br><br>
        <input type="text" id="last-name" name="last-name"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" id="male" name="gender" value="Male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="Female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="Other">
        <label for="other">Other</label><br><br>
        <label for="nationality">Nationality:</label><br><br>
        <select name="nationality" id="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select><br><br>
        <label for="">Language Spoken:</label><br><br>
        <input type="checkbox" id="b-indonesia" name="bahasa-indonesia" value="Bahasa Indonesia">
        <label for="b-indonesia"> Bahasa Indonesia</label><br>
        <input type="checkbox" id="b-english" name="english" value="English">
        <label for="b-english"> English</label><br>
        <input type="checkbox" id="b-other" name="other" value="Other">
        <label for="b-other"> Other</label><br><br>
        <label for="bio">Bio:</label><br><br>
        <textarea id="bio" name="bio" rows="10" cols="30"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>

</html>