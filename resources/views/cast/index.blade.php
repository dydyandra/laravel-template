@extends('adminlte.master')

@section('content')

<!-- Main content -->

    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <h2 class="card-title">Casts</h2>

            <div class="card-tools">
                <a class="btn btn-primary btn-sm d-inline" href="/cast/create">Create new cast</a>
            </div>
        </div>

        <div class="card-body">
            @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th style="width: 30%">Nama</th>
                        <th style="width: 10%">Umur</th>
                        <th style="width: 40%">Bio</th>
                        <th style="width: 20%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($casts as $key => $cast)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $cast->nama }}</td>
                        <td>{{ $cast->umur }}</td>
                        <td>{{ $cast->bio }}</td>
                        <td class="d-flex">
                            <a class="btn btn-info btn-sm" href="/cast/{{ $cast->id }}">Show</a>
                            <a class="btn btn-primary btn-sm ml-2 " href="/cast/{{ $cast->id }}/edit">Edit</a>
                            <form action="/cast/{{ $cast->id }}" method="POST">
                                @csrf
                                @method("DELETE")
                                <input type="submit" value="Delete" class="btn btn-danger btn-sm ml-2 ">
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td align="center">No Data</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

@endsection
