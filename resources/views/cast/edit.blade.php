@extends('adminlte.master')

@section('content')
<div class="card card-primary mt-3 mx-3">
    <div class="card-header">
        <h3 class="card-title">Edit Form Cast #{{ $cast->id }}</h3>
    </div>

    <form action="/cast/{{ $cast->id }}" method="POST">
        @csrf
        @method("PUT")
        <div class="card-body">
            <div class="form-group">
                <label for="id">ID</label>
                <input readonly type="number" class="form-control" id="id" name="id" value="{{ old('id', $cast->id) }}"
                    placeholder="Masukkan nama">
            </div>
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', $cast->nama) }}"
                    placeholder="Masukkan nama">
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number" class="form-control" id="umur" name="umur" value="{{ old('umur', $cast->umur) }}"
                    placeholder="Masukkan umur">
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio', $cast->bio) }}"
                    placeholder="Masukkan bio">
            </div>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </form>
</div>
@endsection
