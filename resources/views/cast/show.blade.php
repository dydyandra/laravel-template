@extends('adminlte.master')
@section('content')
<div>
    <a class="btn btn-primary btn-m ml-3 mt-3" href="{{ url()->previous() }}"><i class="fas fa-arrow-left"></i></a>
</div>

<div class="card mx-3 mt-3">
    <div class="card-header">
        <h3 class="card-title">{{ $cast->nama }}</h3>
    </div>

    <div class="card-body">
        {{-- <p><strong>Cast ID   : </strong>{{ $cast->id}}</p>
        <p><strong>Nama   : </strong>{{ $cast->nama }}</p> --}}
        <p><strong>Umur   : </strong>{{ $cast->umur }}</p>
        <p><strong>Bio    : </strong><br>{{ $cast->bio }}</p>
    </div>
    <!-- /.card-body -->
</div>
@endsection
