@extends('adminlte.master')

@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Create Cast</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="/cast" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama','') }}"
                    placeholder="Masukkan nama">
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number" class="form-control" id="umur" name="umur" value="{{ old('umur','') }}"
                    placeholder="Masukkan umur">
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio','') }}"
                    placeholder="Masukkan bio">
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>
    </form>
</div>
@endsection
